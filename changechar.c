#include <stdio.h>

void changechara(char *x);

int main()
{
    printf("\nTuliskan kalimat : ");

    // char kalimat[100] = "Polytron memang canggih euy!";

    char kalimat[100];
    //scanf("%[^\n]", &kalimat);
    gets(kalimat);

    char *p = kalimat;

    changechara(p);

    printf("\n%s", kalimat);

    printf("\n");
    return 0;
}

void changechara(char *x)
{
    while (*x != '\0')
    {
        if (*x == 'a' || *x == 'A')
            *x = '4';
        else if (*x == 'i' || *x == 'I')
            *x = '1';
        else if (*x == 'u' || *x == 'U')
            *x = '7';
        else if (*x == 'e' || *x == 'E')
            *x = '3';
        else if (*x == 'o' || *x == 'O')
            *x = '0';

        *x++;
    }
}