#include <stdio.h>

int main()
{
    int marks[5] = {30, 35, 25, 40, 46};

    // pointer to oth element
    int *p = marks;

    // Pointer to an array of 5 integers
    int(*ptr)[5] = &marks;

    printf("%u\n%u\n", p, ptr);
    printf("\n*p:%d\n*ptr:%u\n**ptr: %d\n", *p, *ptr, **ptr);

    return 0;
}