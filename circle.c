#include <stdio.h>
#include <math.h>

#define PI 3.142857

void circlearea(double x, double *y);

int main()
{
    double radius, area;
    printf("\nEnter a Radius : ");
    scanf("%lf", &radius);

    circlearea(radius, &area);

    printf("\nThe Circle Area is : ");
    printf("%lf", area);

    printf("\n");
    return 0;
}

void circlearea(double x, double *y)
{
    *y = PI * pow(x, 2);
    //printf("%lf", *y);
}