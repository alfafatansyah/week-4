// Latihan

#include <stdio.h>
#include <stdlib.h>

struct node
{
    char data;
    struct node *next;
};

void printkata(struct node *n)
{
    while (n != NULL)
    {
        printf(" %c ", n->data);
        n = n->next;
    }
}

void addfront(struct node **head_ref, char new_data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));

    temp->data = new_data;
    temp->next = (*head_ref);
    (*head_ref) = temp;
}

void addlast(struct node **head_ref, char new_data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    struct node *last = *head_ref;

    temp->data = new_data;
    temp->next = NULL;

    if (*head_ref == NULL)
    {
        *head_ref = temp;
        return;
    }

    while (last->next != NULL)
        last = last->next;

    last->next = temp;
    return;
}

void addafter(struct node *prev_node, char new_data)
{
    if (prev_node == NULL)
    {
        printf("the given previous node cannot be NULL");
        return;
    }

    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = new_data;
    temp->next = prev_node->next;
    prev_node->next = temp;
}

void delete (struct node *head_ref, int pos)
{
    struct node *temp = head_ref;

    if (pos == 0)
    {
        printf("\nElement deleted is : %c\n", temp->data);
        *head_ref = *head_ref->next;
        temp->next = NULL;
        free(temp);
    }
    else
    {
        for (int i = 0; i < pos - 1; i++)
        {
            temp = temp->next;
        }
        struct node *del = temp->next;
        temp->next = temp->next->next;
        printf("\nElement deleted is : %c\n", del->data);
        del->next = NULL;
        free(del);
    }
}

int main()
{
    struct node *head = NULL;
    struct node *body = NULL;
    struct node *tail = NULL;

    head = (struct node *)malloc(sizeof(struct node));
    body = (struct node *)malloc(sizeof(struct node));
    tail = (struct node *)malloc(sizeof(struct node));

    head->data = 'O';
    head->next = body;

    body->data = 'Y';
    body->next = tail;

    tail->data = 'R';
    tail->next = NULL;

    addfront(&head, 'P');

    addlast(&head, 'O');

    addafter(head->next, 'L');

    addlast(&head, 'N');

    printkata(head);



    delete (head, 0);

    printkata(head);

    return 0;
}