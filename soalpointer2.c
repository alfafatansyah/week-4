#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, num;
    float *data;

    do
    {
        printf("Masukan Jumlah Angka Yang Di Inginkan[1-10]:");
        scanf("%d", &num);

        if (num < 0 || num > 10)
            printf("angka melebihi 10");

        else
        {
            data = (float *)calloc(num, sizeof(float));
            if (data == NULL)
                break;

            for (i = 0; i < num; ++i)
            {
                printf("Masukan Angka %d: ", i + 1);
                scanf("%f", data + i);
            }

            for (i = 1; i < num; ++i)
            {
                if (*data < *(data + i))
                    *data = *(data + i);
            }

            printf("\nAngka Terbesar Adalah = %.2f", *data);

            free(data);
            break;
        }

    } while (num != 999);

    return 0;
}