#include <stdio.h>
#include <string.h>

typedef struct student
{
    char nama[20];
    int umur;
} siswa;

void change(siswa *a);

int main()
{
    siswa s1, *s2;

    strcpy(s1.nama, "roronoa zoro" );
    s1.umur = 30;

    printf("\n%s umur %d", s1.nama, s1.umur);

    s2 = &s1;
    change(&s1);

    printf("\n%s umur %d", s1.nama, s1.umur);

    return 0;
}

void change(siswa *a){
    strcpy(a->nama, "monkey d luffy" );
    a->umur = 20;
}