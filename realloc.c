#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int n1 = 10;
    int n2 = 30;

    char *kata;
    // kata = (char *)malloc(n1 * sizeof(char));
    kata = (char *)calloc(n1, sizeof(char));
    printf("\nkata '%s' tersimpan di address %d", kata, *kata);

    strcpy(kata, "polytron");

    printf("\nkata '%s' tersimpan di address %d", kata, *kata);

    kata = realloc(kata, n2 * sizeof(int));
    strcpy(kata, "polytron memang canggih");

    printf("\nkata '%s' tersimpan di address %d", kata, *kata);

    free(kata);

    printf("\nkata '%s' tersimpan di address %d", kata, *kata);

    return 0;
}