#include <stdio.h>

void factorial(int x, int *y);

int main()
{
    /*/ int a = 30;
    // int *b;
    // b = &a;

    int a, b;

    int *c = &a;
    int *d = &b;

    scanf("%d", c);
    scanf("%d", d);

    //*b = 40;
    printf("a = %d, *b = %d", a, b);*/

    int a = 5;
    int b;

    factorial(a, &b);
    printf("\n%d", b);

    return 0;
}

void factorial(int x, int *y)
{
    //y = factorial (x - 1) * x;
    *y = 1;
    for (int i = 0; i < x; i++)
    {
        *y = *y * (x - i);
        //printf("\n%d", *y);
    }    
}
