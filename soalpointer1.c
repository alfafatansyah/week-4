#include <stdio.h>

int main()
{
    int x;
    int y, a, b, c;
    int *px;

    px = &x;
    printf("Masukan Angka Untuk interger x :");
    scanf("%d", &x);
    printf("Nilai x Sekarang Adalah        :%d\n", x);
    printf("Alamat x Adalah                :%p\n", &x);
    printf("Px menunjuk ke                 :%p\n", px);

    y = *px;
    printf("nilai y sekarang menjadi       :%d\n", y);
    printf("alamat y adalah                :%p\n", &y);

    a = *px;
    printf("nilai a sekarang adalah        :%d\n", a);
    printf("alamat a adalah                :%p\n", &a);

    b = *px;
    printf("nilai b sekarang adalah        :%d\n", b);
    printf("alamat b adalah                :%p\n", &b);

    c = *px;
    printf("nilai c sekarang menjadi       :%d\n", c);
    printf("alamat c adalah                :%p\n", &c);

    return 0;
}