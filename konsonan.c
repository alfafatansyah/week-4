#include <stdio.h>

int main()
{
    char kata[20] = "polytron";
    char *p;
    p = kata;

    int vocal = 0;
    int konsonan = 0;

    while(*p != '\0'){
        if (*p == 'a' || *p == 'i' || *p == 'u' || *p == 'e' || *p == 'o')
        vocal++;
        else
        konsonan++;

        p++;
    }

    printf("\nvocal = %d", vocal);
    printf("\nvocal = %d", konsonan);

    return 0;
}