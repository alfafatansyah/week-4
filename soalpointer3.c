#include <stdio.h>
#include <string.h>

void prmt(char *a, int i, int n);
void ganti(char *x, char *y);

int main()
{
    char a[20];
    int n;

    printf("Masukkan sebuah kata [maksimal 4 huruf]: ");
    scanf("%s", a);
    n = strlen(a) - 1;
    printf("\n%d", n);

    if (n > 4)
        printf("Maaf, kata yang anda masukkan melebihi batas");

    else
    {
        printf("Permutasi dari kata tersebut adalah: \n");
        prmt(a, 0, n);
    }

    return 0;
}

void prmt(char *a, int i, int n)
{
    int j;
    if (i == n)
        printf("\n%s", a);

    else
    {
        for (j = i; j <= n; j++)
        {
            ganti((a + i), (a + j));
            prmt(a, (i + 1), n);
            ganti((a + i), (a + j));
        }
    }
}

void ganti(char *x, char *y)
{
    char temp;
    temp = *x;
    *x = *y;
    *y = temp;
}